var lonlat = [2.654472, 48.850572]

var map = new ol.Map({
        target: 'demoMap',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          })
        ],
        view: new ol.View({
          center: ol.proj.fromLonLat(lonlat),
          zoom: 16
        })
      });

var marker = new ol.Feature({
	geometry: new ol.geom.Point(
		ol.proj.fromLonLat(lonlat)
	),
});

var iconStyle = new ol.style.Style({
		image: new ol.style.Icon(({
				anchor: [0.5, 1],
				src: "/static/contact/img/pin.png"
		}))
});

marker.setStyle(iconStyle);

var vectorSource = new ol.source.Vector({
	features: [marker]
});

var markerVectorLayer = new ol.layer.Vector({
	source: vectorSource
});

map.addLayer(markerVectorLayer);
