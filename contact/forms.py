
from captcha.fields import CaptchaField

from django import forms

class ContactForm(forms.Form):
    society = forms.CharField(
        label='Société',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'text',
            'placeholder': 'Votre Société',
            'id': 'society',
        }),
        required=False,
    )
    lastname = forms.CharField(
        label='Nom',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'text',
            'placeholder': 'Votre Nom',
            'id': 'name',
        }),
        required=False,
    )
    firstname = forms.CharField(
        label='Prénom',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'text',
            'placeholder': 'Votre Prénom',
            'id': 'firstname',
        }),
        required=False,
    )
    email = forms.EmailField(
        label='Adresse mail *',
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
            'placeholder': 'Entrez votre addresse mail',
            'id': 'email',
        })
    )
    #phonenumber =
    mail_object = forms.CharField(
        label='Objet *',
        required=True,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Objet',
            'type': 'text',
            'id': 'mail_object',
        }),
    )
    message = forms.CharField(
            label='Message *',
            widget=forms.Textarea(attrs={
                'class': 'form-control',
                #'rows': '5',
                'placeholder': 'Veuillez rédiger votre message ici',
                'id': 'message',
            }),
            required=True,
    )
    captcha = CaptchaField(label='Nous devons nous assurer que vous n’êtes pas un robot')
