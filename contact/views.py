import os
import logging
import environ
import requests

from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.core.mail import EmailMessage
from django.conf import settings

from showcase.models import Subpage

from contact import forms

_logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

env = environ.Env()
env.read_env(env_file=os.path.join(settings.BASE_DIR, '.env'))

# Create your views here.
def contact_view(request):
    form = None
    if request.method == 'POST':
        _logger.info("received POST form")
        form = forms.ContactForm(request.POST)

        #checking if honeypot fields have been completed
        if 'scales' in request.POST.keys() or request.POST['phone_number']:
            _logger.warning('bot tried to submit in form, mail has not been sent')
            for key, value in request.POST.items():
                _logger.debug(f'{key}: {value}')
            return HttpResponseRedirect(reverse_lazy('contact:confirmation_view'))

        if form.is_valid():
            _logger.info('received captcha answer is correct')
            _logger.info("received form is valid")
            firstname = form.cleaned_data['firstname']
            lastname = form.cleaned_data['lastname']
            society = form.cleaned_data['society']
            subject = form.cleaned_data['mail_object']
            message = form.cleaned_data['message']
            from_email = form.cleaned_data['email']
            body=""
            body = body + "Vous avez reçu un message de :\n"
            body = body + "Société : {}\n".format(society if len(society) > 0 else "non renseignée")
            body = body + "Nom : {}\n".format(lastname if len(lastname) > 0 else "non renseigné")
            body = body + "Prénom : {}\n".format(firstname if len(firstname) > 0 else "non renseigné")
            body = body + "Contenu du message : \n\n{}".format(message)
            msg = EmailMessage(
                subject=subject,
                body=body,
                from_email=from_email,
                to=[env('EMAIL_HOST_USER')],
            )
            msg.send()
            _logger.info("mail sent to host")
            confirmation = EmailMessage(
                subject="[confirmation] " + subject,
                body="[Ce message a été généré automatiquement]\nVotre message à Sasu Diag Immo77 a bien été transmis. Veuillez trouver ci-après, une copie du message:\n" + message,
                from_email=env('EMAIL_HOST_USER'),
                to=[from_email],
            )
            confirmation.send()
            _logger.info("confirmation sent to customer")
            return HttpResponseRedirect(reverse_lazy('contact:confirmation_view'))
        else:
            _logger.debug(form.errors)
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        form = forms.ContactForm()
    return render(request,'contact/contact.html',{'form': form, 'subpages': Subpage.objects.all()})

def confirmation_view(request):
    return render(request,'contact/contact_confirmation.html',{'subpages': Subpage.objects.all()})
