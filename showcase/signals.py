from django.utils.text import slugify
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_delete

from showcase.models import Subpage, HomeSlider


@receiver(pre_save, sender=Subpage)
def pre_save_subpage_receiver(sender, instance, *args, **kwargs):
    slug = slugify(instance.title)
    exists = Subpage.objects.filter(slug=slug).exists()
    if exists:
        page = Subpage.objects.get(slug=slug)
        if page.id != instance.id:
            slug = "{}-{}".format(slug, instance.id)
    instance.slug = slug

@receiver(post_delete, sender=HomeSlider)
def post_delete_slide_receiver(sender, instance, **kwargs):
    if instance.slide_image:
        if os.path.isfile(instance.slide_image.path):
            os.remove(instance.slide_image.path)
