from django.shortcuts import render
from . import models

# Create your views here.
def home(request):
    context = {
        'subpages': models.Subpage.objects.all(),
	    'slider': models.HomeSlider.objects.all(),
    }
    return render(request, 'showcase/home.html', context)

def subpage(request, slug='default-slug'):
    subpage = models.Subpage.objects.get(slug=slug)
    context = {
        'content': subpage,
        'subpages': models.Subpage.objects.all(),
    }
    return render(request, 'showcase/subpage.html', context)

def credits_view(request):
    return render(request, 'showcase/credits.html')
