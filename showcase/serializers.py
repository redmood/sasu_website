from rest_framework import serializers
from showcase.models import Subpage


class SubpageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subpage
        fields = ['title', 'menu_id', 'slug', 'content', 'html_filename', 'image_caption', 'image_subcaption']
