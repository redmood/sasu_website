import logging

from filer.fields.image import FilerImageField
from filer.fields.file import FilerFileField

from django.db import models
from django.conf import settings
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

_logger = logging.getLogger(__name__)

class PageWithHeaderMixin(models.Model):
    class Meta:
        abstract=True

    H_DPE = 'header-dpe.jpg'
    H_ELECTRICITE = 'header-electricite.jpg'
    H_GAZ = 'header-gaz.jpg'
    H_LOCATION = 'header-location.jpg'
    H_BOUTIN = 'header-loi-boutin.jpg'
    H_CARREZ = 'header-loi-carrez.jpg'
    H_PLOMB = 'header-plomb.jpg'
    H_TERMITES = 'header-termites.jpg'
    H_VENTE = 'header-vente.jpg'
    H_SLIDE1 = 'slide1.jpg'
    H_SLIDE2 = 'slide2.jpg'
    H_SLIDE3 = 'slide3.jpg'
    H_SLIDE4 = 'slide4.jpg'
    H_AMIANTE = 'header-amiante.jpg'

    MENU_IMAGE_CHOICES = [
        (H_DPE, 'dpe'),
        (H_ELECTRICITE, 'electricite'),
        (H_GAZ, 'gaz'),
        (H_LOCATION, 'location'),
        (H_BOUTIN, 'loi-boutin'),
        (H_CARREZ, 'loi-carrez'),
        (H_PLOMB, 'plomb'),
        (H_TERMITES, 'termites'),
        (H_VENTE, 'vente'),
        (H_SLIDE1, 'slide 1'),
        (H_SLIDE2, 'slide 2'),
        (H_SLIDE3, 'slide 3'),
        (H_SLIDE4, 'slide 4'),
        (H_AMIANTE, 'amiante'),
    ]

    header_image=FilerImageField(
        verbose_name=_('slide image'),
        on_delete=models.CASCADE,
        null=True,
    )
    image_caption=models.CharField(
        max_length=250,
        null=False,
        blank=True,
    )
    image_subcaption=models.CharField(
        max_length=250,
        null=False,
        blank=True,
    )


class HomeSlider(PageWithHeaderMixin):

    def __repr__(self):
        return self.image_caption

    class Meta:
        verbose_name=_('Home Slide')
        verbose_name_plural=_('Home Slides')


# Create your models here.
class Subpage(PageWithHeaderMixin):
    class Meta:
        verbose_name=_('Subpage')
        verbose_name_plural=_('Subpages')

    DIAGS = 'diagnostiques'
    OBS = 'obligations'

    MENU_ID_CHOICES = [
        (DIAGS, 'Les diagnostiques'),
        (OBS, 'Vos obligations'),
    ]

    HTML_OBLIGATIONS_VENTE = 'obligations-vente.html'
    HTML_OBLIGATIONS_LOCATION = 'obligations-location.html'

    MENU_HTML_FILE_CHOICE = [
        (HTML_OBLIGATIONS_VENTE, 'obligations vente'),
        (HTML_OBLIGATIONS_LOCATION, 'obligations location'),
    ]

    menu_logo=FilerImageField(
        related_name='menu_logo',
        verbose_name=_('Page logo'),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    title=models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_('title')
    )

    menu_id=models.CharField(
        max_length=50,
        null=False,
        blank=False,
        choices=MENU_ID_CHOICES,
        default=DIAGS,
    )

    slug=models.SlugField(
        unique=True,
        max_length=50,
        null=True,
        blank=True,
    )

    content=models.TextField(
        null=False,
        blank=True,
        verbose_name=_('content'),
    )

    html_content=models.CharField(
        max_length=255,
        verbose_name=_('HTML file name'),
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title
