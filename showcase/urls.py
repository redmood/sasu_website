from django.urls import path
from showcase import views

app_name = 'showcase'

urlpatterns = [
	path('', views.home, name='home'),
	path('page/<slug:slug>', views.subpage, name='subpage'),
	path('credits', views.credits_view, name='credits')
]
