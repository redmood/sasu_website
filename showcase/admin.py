from ckeditor_uploader.widgets import CKEditorUploadingWidget

from django.contrib import admin
from django.db.models import TextField
from django.utils.translation import gettext_lazy as _

from showcase.models import Subpage, HomeSlider
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin


admin.site.unregister(User)
@admin.register(User)
class ShowcaseUserAdmin(UserAdmin):
    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(ShowcaseUserAdmin, self).get_readonly_fields(request, obj)

        if request.user.is_superuser:
            return readonly_fields

        new_readonly_fields = ('last_login', 'date_joined')
        return new_readonly_fields

    def get_fieldsets(self, request, obj=None):
        fieldsets = super(ShowcaseUserAdmin, self).get_fieldsets(request, obj)

        if request.user.is_superuser:
            return fieldsets

        new_fieldsets = [(title, panel) for title, panel in fieldsets if title != 'Permissions']
        return new_fieldsets

    def get_queryset(self, request):
        queryset = super(ShowcaseUserAdmin, self).get_queryset(request)

        if request.user.is_superuser:
            return queryset

        return queryset.filter(id=request.user.id)


@admin.register(Subpage)
class SubpageAdmin(admin.ModelAdmin):
    class Meta:
        model = Subpage
    list_display = ['title', 'menu_id', 'image_caption', 'image_subcaption']
    formfield_overrides = {
        TextField: {'widget': CKEditorUploadingWidget()},
    }
    fieldsets = (
        (_('General'), {
            'fields': ('title', 'menu_id', 'menu_logo')
        }),
        (_('Main Content'), {
            'fields': ('header_image', 'image_caption', 'image_subcaption', 'content')
        }),
        (_('Optional Content'), {
            'fields': ('html_content',)
        })
    )
    list_filter = ('menu_id',)


@admin.register(HomeSlider)
class HomeSliderAdmin(admin.ModelAdmin):
    list_display = ['header_image', 'image_caption', 'image_subcaption']
