# Generated by Django 2.2.3 on 2019-10-21 06:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HomeSlider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header_image', models.CharField(choices=[('header-dpe.jpg', 'dpe'), ('header-electricite.jpg', 'electricite'), ('header-gaz.jpg', 'gaz'), ('header-location.jpg', 'location'), ('header-loi-boutin.jpg', 'loi-boutin'), ('header-loi-carrez.jpg', 'loi-carrez'), ('header-plomb.jpg', 'plomb'), ('header-termites.jpg', 'termites'), ('header-vente.jpg', 'vente'), ('slide1.jpg', 'slide 1'), ('slide2.jpg', 'slide 2'), ('slide3.jpg', 'slide 3'), ('slide4.jpg', 'slide 4'), ('header-amiante.jpg', 'amiante')], max_length=25, null=True)),
                ('image_caption', models.CharField(blank=True, max_length=250)),
                ('image_subcaption', models.CharField(blank=True, max_length=250)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Subpage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header_image', models.CharField(choices=[('header-dpe.jpg', 'dpe'), ('header-electricite.jpg', 'electricite'), ('header-gaz.jpg', 'gaz'), ('header-location.jpg', 'location'), ('header-loi-boutin.jpg', 'loi-boutin'), ('header-loi-carrez.jpg', 'loi-carrez'), ('header-plomb.jpg', 'plomb'), ('header-termites.jpg', 'termites'), ('header-vente.jpg', 'vente'), ('slide1.jpg', 'slide 1'), ('slide2.jpg', 'slide 2'), ('slide3.jpg', 'slide 3'), ('slide4.jpg', 'slide 4'), ('header-amiante.jpg', 'amiante')], max_length=25, null=True)),
                ('image_caption', models.CharField(blank=True, max_length=250)),
                ('image_subcaption', models.CharField(blank=True, max_length=250)),
                ('title', models.CharField(max_length=50)),
                ('menu_id', models.CharField(choices=[('diagnostiques', 'Les diagnostiques'), ('obligations', 'Vos obligations')], default='diagnostiques', max_length=50)),
                ('slug', models.SlugField(blank=True, null=True, unique=True)),
                ('html_content', models.TextField(blank=True)),
                ('markdown_filename', models.CharField(blank=True, choices=[('diagnostic-amiante.md', 'amiante'), ('diagnostic-electricite.md', 'electricite'), ('diagnostic-plomb.md', 'plomb'), ('diagnostic-termites.md', 'termites'), ('diagnostic-gaz.md', 'gaz'), ('loi-boutin.md', 'boutin'), ('loi-carrez.md', 'carrez'), ('obligations-location.md', 'location'), ('obligations-vente.md', 'vente'), ('performance-energetique.md', 'dpe')], max_length=30, null=True)),
                ('html_filename', models.CharField(blank=True, choices=[('obligations-vente.html', 'obligations vente'), ('obligations-location.html', 'obligations location')], max_length=30, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
