(function($) {
  "use strict"; // Start of use strict

  // Floating label headings for the contact form
  $("body").on("input propertychange", ".floating-label-form-group", function(e) {
    $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
  }).on("focus", ".floating-label-form-group", function() {
    $(this).addClass("floating-label-form-group-with-focus");
  }).on("blur", ".floating-label-form-group", function() {
    $(this).removeClass("floating-label-form-group-with-focus");
  });

  // Show the navbar when the page is scrolled up
  var MQL = 780;

  //primary navigation slide-in effect
  if ($(window).width() > MQL) {
    var headerHeight = $('#mainNav').height();
    $(window).on('scroll', {
        previousTop: 0
      },
      function() {

				var currentTop = $(window).scrollTop();
				//if below header
				if (currentTop > 3 * headerHeight) {
					//console.log("must show menu")
					if (!$('#mainNav').hasClass('is-fixed')) $('#mainNav').addClass('is-fixed');
					if (!$('#mainNav').hasClass('is-visible')) $('#mainNav').addClass('is-visible');
				} else if (currentTop < headerHeight) {
					//console.log("must put menu back on top")
					if ($('#mainNav').hasClass('is-visible')) $('#mainNav').removeClass('is-visible');
					if ($('#mainNav').hasClass('is-fixed')) $('#mainNav').removeClass('is-fixed');
				}

      });
  }

})(jQuery); // End of use strict
