var slides = document.getElementsByClassName('slide')
var pos = 1
console.log(slides)

for (var i = 0; i < slides.length; i++) {
	slides[i].style.display = "none"
}

if (slides.length == 1) {
	slides[0].style.display = "block"
} else if (slides.length > 1) {
	slides[0].style.display = "block";
	var interval = setInterval(function() {
		//console.log('sliding')
		//console.log(pos)
		if (pos < 0 || pos >= slides.length) {
			pos = 0
		}

		slides[pos].style.display = "block"

		for (var i = 0; i < slides.length; i ++) {
			if (i != pos) slides[i].style.display = "none"
		}

		pos = pos + 1
		if (pos >= slides.length) pos = 0
	}, 4096)
}
