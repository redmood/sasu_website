from sasu_website.settings.base import *

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'diag_immo_db',
		'USER': 'dev_user',
		'PASSWORD': 'dev_password',
		'HOST': 'localhost',
	}
}
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
